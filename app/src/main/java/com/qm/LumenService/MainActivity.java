package com.qm.LumenService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tvLxShower;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvLxShower = (TextView) findViewById(R.id.tv_lx_shower);
        registerReceiver(receiver, new IntentFilter(HorrorService.CHANNEL));

        Intent svc=new Intent(this, HorrorService.class);
        startService(svc);
    }

    protected BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            float light = intent.getFloatExtra(HorrorService.BRIGHTNESS, 1);
            tvLxShower.setText(Float.toString(light));
        }
    };
}