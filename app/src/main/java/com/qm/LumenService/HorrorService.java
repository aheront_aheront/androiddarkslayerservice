package com.qm.LumenService;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class HorrorService extends Service implements SensorEventListener {
    public static final String CHANNEL = "HORROR_SERVICE";
    public static final String BRIGHTNESS = "BRIGHTNESS";

    private MediaPlayer music;

    private SensorManager sensorManager;
    private Sensor light;

    @Override
    public void onCreate() {
        super.onCreate();
        music = MediaPlayer.create(this, R.raw.doom);
        music.setLooping(true);
        music.setVolume(50,50);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        light = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        music.start();
        sensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        music.stop();
        music.release();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        Intent activityCallback = new Intent(CHANNEL);
        activityCallback.putExtra(BRIGHTNESS, sensorEvent.values[0]);
        sendBroadcast(activityCallback);

        Log.d("Horror", sensorEvent.values[0] + "");
        Log.d("Horror", Boolean.toString(music.isPlaying()));

        if(sensorEvent.values[0] <= 1)
            music.start();
        else
            music.pause();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {


    }
}